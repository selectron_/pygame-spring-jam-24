from pygame import NUMEVENTS
from pygame.event import custom_type, event_name


event_listeners = dict([(i, []) for i in range(NUMEVENTS) if event_name(i) != event_name(NUMEVENTS)])
'''add user events here'''
BLOCK_EVENT     = custom_type() #"btype": eventid, "exc": [obj1, obj2, ...]
ALLOW_EVENT     = custom_type() #"atype": eventid, "exc": [obj1, obj2, ...]

#colors
MISSING = 255, 0, 255