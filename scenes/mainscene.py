from .baseclasses import Scene
from typing import TYPE_CHECKING
from level.level import Level

if TYPE_CHECKING:
    from .scenes import Scenes
    from pygame import Surface
    from threading import Semaphore


class MainScene(Scene):
    def __init__(self, scenes:"Scenes", init_done:"Semaphore") -> None:
        super().__init__(scenes)
        self.level = Level()
        init_done.release(n=10) #just in case i forget someone still waiting for the init
    

    def update(self) -> None:
        self.level.update()

    
    def draw(self, surface: "Surface") -> None:
        self.level.draw(surface)