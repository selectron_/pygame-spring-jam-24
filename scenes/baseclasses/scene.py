from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Surface
    from pygame.event import Event
    from scenes.scenes import Scenes

class Scene():
    def __init__(self, scenes:"Scenes") -> None:
        self.switch_to  = None
        self.scenes     = scenes


    def getSwitchTo(self) -> 'Scene':
        return_val = self.switch_to
        self.switch_to = None
        return return_val


    def onEvent(self, event:"Event") -> None:
        pass


    def update(self, *args: any, **kwargs: any) -> None:
        pass


    def draw(self, surface:"Surface") -> None:
        pass


