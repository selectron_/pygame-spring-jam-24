#!/bin/bash 

app_name="game"

if test -d build; then rm -r build; fi 
if test -d dist; then rm -r dist; fi
if test -f $app_name.spec; then rm $app_name.spec; fi 

pyinstaller --clean --noconfirm --onefile --windowed --name "$app_name" "main.py"
