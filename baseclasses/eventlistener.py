from constants import event_listeners, BLOCK_EVENT, ALLOW_EVENT
from abc import ABC, abstractmethod
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame.event import Event, EventType

class EventListener(ABC):
    def __init__(self) -> None:
        self.blocked_events = set()
        self._add_event(BLOCK_EVENT)
        self._add_event(ALLOW_EVENT)


    def _add_event(self, event_type:"EventType"):
        event_listeners[event_type].append(self)


    @abstractmethod
    def onEvent(self, event:"Event"):
        if event.type == BLOCK_EVENT and self not in event.exc:
            self.blocked_events.add(event.btype)

        elif event.type == ALLOW_EVENT and self not in event.exc:
            self.blocked_events.discard(event.atype)