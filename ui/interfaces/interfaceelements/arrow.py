from pygame import Vector2, Surface, SRCALPHA
from pygame.sprite import Sprite
from pygame.transform import rotozoom, rotate
from pygame.draw import line, lines
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame.sprite import Group

class Arrow(Sprite, Vector2):
    @classmethod
    def from_start_end(cls, start:"Vector2", end:"Vector2", group:"Group", color=(255, 0, 0), factor:float=10) -> "Arrow":
        return Arrow(start, end - start, group, color, factor)
    
    
    def __init__(self, pos:"Vector2", value:"Vector2", group:"Group", color=(255, 0, 0), factor:float=10) -> None:
        Sprite.__init__(self, group)
        Vector2.__init__(self, value)

        self.pos = pos
        self.color = color
        self.factor = factor

        self.ref_image = Surface((200, 11), SRCALPHA)
        self.head_ref = Surface((40, 11), SRCALPHA)
        lines(self.head_ref, self.color, True, ((0, 10), (0, 0), (19, 5)))
        self.image = self.ref_image.copy()
        self.rect = self.image.get_rect(center = self.pos)
        self.head_rect = self.head_ref.get_rect(center = self*self.factor + self.pos - self.rect.topleft)
        self.image.blit(self.head_ref, self.head_rect)
        line(self.image, self.color, self.pos - self.rect.topleft, self.pos - self.rect.topleft + self*self.factor)


    @property
    def tip(self) -> "Vector2":
        return self.pos + self*self.factor
    

    def __hash__(self) -> int:
        return Sprite.__hash__(self)


    def update(self) -> None:
        angle = self.angle_to((1, 0))
        factor = (self*self.factor).length()/100
        self.image = rotozoom(self.ref_image, angle, factor)
        self.rect = self.image.get_rect(center = self.pos)
        head = rotate(self.head_ref, angle)
        self.head_rect = head.get_rect(center = self*self.factor + self.pos - self.rect.topleft)
        self.image.blit(head, self.head_rect)
        line(self.image, self.color, self.pos - self.rect.topleft, self.pos - self.rect.topleft + self*self.factor)

    
    def updateVector(self, vector:"Vector2") -> None:
        Vector2.update(self, vector)

    def updateVectorRelative(self, vector:"Vector2") -> None:
        Vector2.update(self, (vector - self.pos)/self.factor)
    
