from constants import WHITE
from pygame import Surface, SRCALPHA
from ui.baseclasses import PassiveInterfaceElement
from pygame.font import SysFont
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Vector2
    from ui.baseclasses import Interface


class TextBox(PassiveInterfaceElement):
    def __init__(self, interface:"Interface", text:str, size:"Vector2", pos:"Vector2") -> None:
        super().__init__(interface)
        self.image = Surface(size, SRCALPHA)
        self.rect = self.image.get_rect(topleft = pos)

        self.text = None
        self.font = SysFont(None, 25)
        self.lineheight = self.font.get_linesize()
        
        self.updateText(text)

    def updateText(self, text:str):
        if text == self.text:
            return
        
        self.text = text
        words = text.split()

        line = 0
        words_len = len(words)

        line_start_i = 0
        line_end_i = words_len
        while line_start_i < words_len:
            if self.font.size((text_joined := " ".join(words[line_start_i:line_end_i])))[0] > self.rect.width:
                line_end_i -= 1
            else:
                self.image.blit(self.font.render(text_joined, True, WHITE), (0, line * self.lineheight))
                line_start_i = line_end_i
                line_end_i = words_len
                line += 1