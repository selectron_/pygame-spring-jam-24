from constants import CYAN, GREEN, RED, BLUE
from pygame import MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEMOTION
from pygame.font import SysFont
from ui.baseclasses import ActiveInterfaceElement
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Vector2
    from pygame.event import Event
    from ui.baseclasses import Interface


class Link(ActiveInterfaceElement):
    def __init__(self, interface:"Interface", pos:"Vector2", size:int, text:str = "") -> None:
        super().__init__(interface)

        self._add_event(MOUSEBUTTONDOWN)
        self._add_event(MOUSEBUTTONUP)
        self._add_event(MOUSEMOTION)

        font = SysFont(None, size)
        self.text_render_plain = font.render(text, True, CYAN)
        self.text_render_highlighted = font.render(text, True, GREEN)
        self.text_render_depressed = font.render(text, True, BLUE)
        self.text_render_forbidden = font.render(text, True, RED)
        self.image = self.text_render_plain
        self.rect = self.image.get_rect(topleft = pos)


    
    def onEvent(self, event:"Event"):
        super().onEvent(event)   

        if self.forbidden: return  

        if event.type == MOUSEBUTTONDOWN and not self in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.image = self.text_render_depressed

        elif event.type == MOUSEBUTTONUP and not self in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.image = self.text_render_plain
                self.active = True

        elif event.type == MOUSEMOTION and not self in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.image = self.text_render_highlighted
            else:
                self.image = self.text_render_plain


    def update(self) -> None:
        if self.forbidden:
            self.image = self.text_render_forbidden
