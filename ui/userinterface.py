from constants import TRANS
from pygame import Surface, SRCALPHA
from pygame.display import get_window_size
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Surface
    from ui.baseclasses import Interface


class UserInterface():
    def __init__(self) -> None:
        self.image = Surface(get_window_size(), SRCALPHA)
        self.rect = self.image.get_rect()

        self.interfaces: list["Interface"] = []

    
    def update(self):
        for interface in self.interfaces:
            interface.update()


    def draw(self, surface:"Surface"):
        self.image.fill(TRANS)
        for interface in self.interfaces:
            interface.draw(self.image)

        surface.blit(self.image, self.rect)