from pygame.sprite import Group, Sprite
from baseclasses import EventListener


class PassiveInterfaceElement(Sprite):
    def __init__(self, interface:"Group") -> None:
        super().__init__(interface)
        self.active = False


class ActiveInterfaceElement(EventListener, Sprite):
    def __init__(self, interface:"Group") -> None:
        Sprite.__init__(self, interface)
        EventListener.__init__(self)
        self.active = False
        self.forbidden = False

    
    def activate(self):
        self.active = True
        return True

    def deactivate(self):
        self.active = False
        return True
        
    def forbid(self):
        self.forbidden = True
        return True

    def allow(self):
        self.forbidden = False
        return True
