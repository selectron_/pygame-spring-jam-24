from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Surface

class InterfaceCollection:
    def __init__(self) -> None:
        self.interfaces = []
        self.active = False
        self.visible = False

    def update(self, *args, **kwargs):
        if not self.active:
            return
        for interface in self.interfaces:
            interface.update(*args, **kwargs)

    def draw(self, surface:"Surface"):
        if not self.visible:
            return
        for interface in self.interfaces:
            interface.draw(surface)


    def activate(self):
        self.active = True
        self.visible = True
        for interface in self.interfaces:
            interface.active = True

    
    def deactivate(self):
        self.active = False
        self.visible = False
        for interface in self.interfaces:
            interface.active = False