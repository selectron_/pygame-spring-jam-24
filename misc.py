from constants import G
from math import sqrt, log10, pi
from random import choices, randint
from pygame import Vector2


from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from solarsystem.celestialobject import *


_prefixes = {-30: "q", -27: "r", -24: "y", -21: "z", -18: "a", -15: "f", -12: "p", -9: "n", -6: "u", -3: "m", 
            0: "", 3: "k", 6: "M", 9: "G", 12: "T", 15: "P", 18: "E", 21: "Z", 24: "Y", 27: "R", 30: "Q"}


_letters = list(map(chr, range(ord('A'), ord('Z')+1)))
_digits = list(map(str, range(10)))

def num_with_prefix(num:float|int) -> str:
    if num != 0:
        exponent = int(3 * int(log10(abs(num)) / 3))
    
        prefix = _prefixes.get(exponent, "x10^" + str(exponent))
        value = num / (10 ** exponent)

        return f"{round(value, 2)}{prefix}"
    return "0"


def random_coords(n:int, range_x:tuple[int, int], range_y:tuple[int, int]):
    return [(randint(range_x[0], range_x[1]), randint(range_y[0], range_y[1])) for _ in range(n)]


def letters_then_digits(n_letters:int, n_digits:int, seperate:str="-", join_letters_by:str="", join_digits_by:str="") -> str:     
    return f"{letters(n_letters, join_letters_by)}{seperate}{digits(n_digits, join_digits_by)}" 

def digits_then_letters(n_digits:int, n_letters:int, seperate:str="-", join_digits_by:str="", join_letters_by:str="") -> str:     
    return f"{digits(n_digits, join_digits_by)}{seperate}{_letters(n_letters, join_letters_by)}" 

def letters_and_digits(n_digits:int, join_by:str="") -> str:     
    return f"{join_by.join(choices(_letters + _digits, k=n_digits))}"

def letters(n_letters:int, join_by:str="") -> str:     
    return f"{join_by.join(choices(_letters, k=n_letters))}"

def digits(n_digits:int, join_by:str="") -> str:     
    return f"{join_by.join(choices(_digits, k=n_digits))}"


def calc_g_force(obj1:"CelestialObject", obj2:"CelestialObject", dist_pow_two:float=None) -> float:
    '''how mutch gravitational force obj1 experiences from obj2'''
    if dist_pow_two is None: 
        dist_pow_two = dist_pow_two = obj1.pos.distance_squared_to(obj2.pos)
    
    return (obj2.pos - obj1.pos).scale_to_length(G * (obj1.mass * obj2.mass) / max(dist_pow_two, 1))


def calc_energy_received(obj1:"CelestialObject", obj2:"CelestialObject", dist_pow_two:float=None) -> float:
    '''how much energy obj1 receives from obj2'''
    if dist_pow_two is None: 
        dist_pow_two = dist_pow_two = obj1.pos.distance_squared_to(obj2.pos)
    
    return obj2.energy/(2 * pi * dist_pow_two)


def calc_orbital_vel(obj1:"CelestialObject", obj2:"CelestialObject", dist:int=None) -> int:
    '''obj1 orbits obj2'''
    if dist is None: 
        dist = obj1.pos.distance_to(obj2.pos)
    
    return sqrt((G * (obj1.mass + obj2.mass)) / dist)


def calc_orbital_vec(obj1:"CelestialObject", obj2:"CelestialObject", dist:int=None) -> "Vector2":
    '''obj1 orbits obj2'''
    if dist is None: 
        dist = obj1.pos.distance_to(obj2.pos)
    
    return calc_orbital_vel(obj1, obj2, dist) * perpendicular_ip((obj1.pos - obj2.pos).normalize())


def perpendicular(vec:"Vector2") ->"Vector2":
    '''return new vector rotated by 90°'''
    return Vector2(vec.y, -vec.x)


def perpendicular_ip(vec:"Vector2") ->"Vector2":
    '''return vector rotated by 90°'''
    vec.update(vec.y, -vec.x)
    return vec