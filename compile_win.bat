set app_name=game
IF EXIST build RMDIR /S /Q build
IF EXIST dist RMDIR /S /Q dist
IF EXIST %app_name%.spec DEL /S /Q %app_name%.spec
pyinstaller --clean --noconfirm --onedir --windowed -n "%app_name%" "src/main.py"